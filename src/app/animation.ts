import {
    animate,
    state,
    style,
    transition,
    trigger
} from "@angular/animations";

export const FLY_IN_OUT_ANIMATION = [
    trigger('flyInOut', [
        state('in', style({transform: 'translateX(100)'})),
        // state('*', style({display: 'inherit'})),
        transition('void => animate', [
            style({transform: 'translateX(-100%)'}),
            animate(2000)
        ]),
        transition('animate => void', [
            animate(4400, style({transform: 'translateX(100%)'}))
        ])
    ])
];

export const ANIMATIONS = [].concat(FLY_IN_OUT_ANIMATION);