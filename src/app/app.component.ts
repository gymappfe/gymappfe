import { Component, ViewChild, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CoursesModalComponent } from './courses-modal/courses-modal.component';
import { TermsConditionsComponent } from './terms-conditions-modal/terms-conditions.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  title = 'app';
  //http://54.70.10.6/api/doc
  //dummy_user@test.com
  //megaSecretTok3n
  team: string[] = ['costin', 'alex', 'razvan', 'vlad'];
  private USERS_COURSES_FILTER = "usersCourses";
  private OWNED_COURSES_FILTER = "ownedCourses";
  private TERMS_CONDITIONS_FILTER = "termsConditions";


  constructor(public dialog: MatDialog){}
  openSubscribedCoursesDialog(): void {
    let dialogRef = this.dialog.open(CoursesModalComponent, {
      height: "60%",
      width: "40%",
      data: {
        filter: this.USERS_COURSES_FILTER,
        canRemove: true
      }
    });
  }

  openOwnedCoursesDialog(): void {
    let dialogRef = this.dialog.open(CoursesModalComponent, {
      height: "60%",
      width: "40%",
      data: {
        filter: this.OWNED_COURSES_FILTER,
        canRemove: false
      }
    });
  }
  openTermsConditionsDialog(): void {
    let dialogRef = this.dialog.open(TermsConditionsComponent, {
      height: "60%",
      width: "40%",
      data: {
        filter: this.TERMS_CONDITIONS_FILTER
      }
    });
  }
}

