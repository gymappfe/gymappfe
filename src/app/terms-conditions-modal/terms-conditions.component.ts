import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TermsConditionsComponent implements OnInit {

  private _filter: string;

  constructor(private _dialogRef: MatDialogRef<TermsConditionsComponent>
    , @Inject(MAT_DIALOG_DATA) dataBundle){
      this._filter = dataBundle.filter;
    }


 

  ngOnInit() {
  }

  onNoClick(): void {
    this._dialogRef.close();
 }

}
