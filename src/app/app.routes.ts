import { Routes } from '@angular/router';
import { LoginComponent } from './login-component/login-component.component';
import { RegisterComponent } from './register.page/register.component';
import { MainComponent } from './main.page/main/main.component';
import { AuthGuard } from './login-component/auth.guard';
import { CourseListComponent } from './main.page/course-list/course-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoggedGuard } from './login-component/logged.guard';
import { ProductsListComponent } from './main.page/products-list/products-list.component';
import { CourseCreateComponent } from './course-create/create-course.component';


export const appRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
      /*   canActivate: [LoggedGuard] */
    },
    {
        path: 'register',
        component: RegisterComponent,
        canActivate: [LoggedGuard]
    },
    {
        path: 'main',
        component: MainComponent,
        
        children: [
            {
                path: 'courses',
                component: CourseListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: 'products',
                component: ProductsListComponent
            },
            {
                path: 'create-course',
                component: CourseCreateComponent
            }
        ]
    },
    {
        path: '',
        redirectTo: '/main',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: NotFoundComponent,
    }
]
