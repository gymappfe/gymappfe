import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

const USER = "ROLE_USER";
const ADMIN = "ROLE_ADMIN";
const TRAINER = "ROLE_TRAINER";


@Injectable()
export class AppService {


  USER_ROLE: string = "";

  constructor(private _cookieService: CookieService) {
    this.initUserRole();
   }

   initUserRole(){
     this.USER_ROLE = this.getUserRole();
   }

  getUserRole() {
    if (this.USER_ROLE !== "") {
      return this.USER_ROLE;
    } else {
      return this._cookieService.get("UserRole");
    }
  }

  setUserRole(userRole: string) {
    let now = new Date();
    now.setTime(now.getTime() + 1 * 3600 * 1000).toLocaleString();
    this._cookieService.set("UserRole", userRole, now);
    this.USER_ROLE = userRole;
  }

  isUser(): boolean {
    return this.USER_ROLE === USER;
  }

  isAdmin(): boolean {
    return this.USER_ROLE === ADMIN;
  }

  isTrainer(): boolean {
    return this.USER_ROLE === TRAINER;
  }

  isLogged(): boolean {
    if(this._cookieService.check('token'))
      return true ;
    else 
    return false; 
  }
  

}
