import { JsonProperty } from 'json-typescript-mapper';
import { Trainer } from '../trainer/trainer';

export class Course {
    id: number;
    eventDate: string;
    name: string;
    image: string;
    @JsonProperty('registered_users')
    registeredUsers: number;
    @JsonProperty({ clazz: Trainer })
    trainer: Trainer;
}