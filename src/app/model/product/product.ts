
export class Product {
    id: number;
    price: number;
    description: string;
    image: string;
    name: string;
    category: string;
}