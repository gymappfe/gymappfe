export class Trainer {
    id: number;
    name: string;
    email: string;
    picture: string;
}