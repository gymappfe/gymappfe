import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Inject } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatListModule,
  MatIconModule,
  MatCheckboxModule,
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatSnackBarModule,
  MatDialog,
  MatDialogModule,
  MatFormFieldModule,
  MatSidenavModule,
  MatMenuModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatRadioModule
} from '@angular/material';
import { MatToolbarModule } from '@angular/material/toolbar';

import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes';
import { AppComponent } from './app.component';
import { LoginComponent } from './login-component/login-component.component';
import { InputEmailComponent } from './login-component/input-email/input-email.component';
import { InputPasswordComponent } from './login-component/input-password/input-password.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { AppConfig } from './config/app-config';
import { AuthGuard } from './login-component/auth.guard';
import { CoursePreviewListComponent } from './components/course-preview-list/course-preview-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RegisterComponent } from './register.page/register.component';
import { NavbarComponent } from './main.page/navbar/navbar.component';
import { FooterComponent } from './main.page/footer/footer.component';
import { CourseListComponent } from './main.page/course-list/course-list.component';
import { UserComponent } from './main.page/user/user.component';
import { CoursesModalComponent } from './courses-modal/courses-modal.component';
import { MainComponent } from './main.page/main/main.component';
import { UserUpdateComponent } from './main.page/user/user-update/user-update.component';
import { LoggedGuard } from './login-component/logged.guard';
import { SafeUrlPipe } from './pipes/safe-url.pipe';
import { ProductsListComponent } from './main.page/products-list/products-list.component';
import { ProductDetailsComponent } from './main.page/products-list/product-details/product-details.component';
import { AppService } from './app.service';
import { CountUpModule } from 'countup.js-angular2';
import { Angular2ImageGalleryModule } from 'angular2-image-gallery';
import { CourseCreateComponent } from './course-create/create-course.component';
import { CounterComponent } from './main.page/main/counter/counter.component';
import { NewsComponent } from './main.page/main/news/news.component';
import { TermsConditionsComponent } from './terms-conditions-modal/terms-conditions.component';


  

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InputEmailComponent,
    InputPasswordComponent,
    NotFoundComponent,
    RegisterComponent,
    NavbarComponent,
    FooterComponent,
    CourseListComponent,
    UserComponent,
    ProductsListComponent,
    CoursesModalComponent,
    MainComponent,
    UserUpdateComponent,
    SafeUrlPipe,
    CoursePreviewListComponent,
    ProductDetailsComponent,
    CourseCreateComponent,
    CounterComponent,
    NewsComponent,
    TermsConditionsComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    MatListModule,
    MatIconModule,
    MatCheckboxModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatSidenavModule,
    MatMenuModule,
    ReactiveFormsModule,
    FormsModule,
    MatSnackBarModule,
    MatDialogModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatToolbarModule,
    CountUpModule,
    Angular2ImageGalleryModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule
  ],
  entryComponents: [UserUpdateComponent, CoursesModalComponent, ProductDetailsComponent, TermsConditionsComponent],
  providers: [CookieService, AppConfig, AuthGuard, LoggedGuard, AppService],
  bootstrap: [AppComponent]

})
export class AppModule { }
