import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-courses-modal',
  templateUrl: './courses-modal.component.html',
  styleUrls: ['./courses-modal.component.css']
})
export class CoursesModalComponent implements OnInit {
  private _filter: string;
  private _canRemove = true;

  constructor(private _dialogRef: MatDialogRef<CoursesModalComponent>
    , @Inject(MAT_DIALOG_DATA) dataBundle) {
    this._filter = dataBundle.filter;
    this._canRemove = dataBundle.canRemove;
  }

  ngOnInit() {
  }

  onNoClick(): void {
    this._dialogRef.close();
  }

}
