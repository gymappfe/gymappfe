import { Component, OnInit, Output } from '@angular/core';
import { RegisterService } from './register.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [RegisterService]
})
export class RegisterComponent implements OnInit {
  private _email: string;
  private _password: string;
  private _fullName: string;
  private _images: File[];

  constructor(private _registerService: RegisterService, public snackBar: MatSnackBar) { }

  register() {

    var image: File;
    if (this._images) {
      image = this._images[0];
    }
    this._registerService.register(this._email, this._password, this._fullName, image)
      .subscribe(data => 
        this.openSnackBar("Done", "Dismiss")
        ,
      error => this.openSnackBar(error.json().error, "OK")
      );

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
  onChange(event) {
    this._images = event.target.files;
    console.log(this._images);
  }

  ngOnInit() {
  }
}
