import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers } from '@angular/http';


@Injectable()
export class RegisterService {

  BASE_URL = "http://54.70.10.6/api"

  constructor(private _http: Http) {
  }

  register(email: string, password: string, fullName: string, image: File) {
    console.log(email + password + image);
    
    let formData = new FormData();
    formData.append("email", email);
    formData.append("password", password);
    formData.append("fullName", fullName);
    if (image) {
      formData.append("picture", image);
    }
    console.log(formData);
    

    return this._http.post(`${this.BASE_URL}/register`, formData)
      .map(res => res.json());
  }

}
