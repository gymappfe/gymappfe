import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { MatSnackBar } from '@angular/material';
import { CookieService } from "ngx-cookie-service";

import { FLY_IN_OUT_ANIMATION } from '../animation'
import { Router } from '@angular/router';
import { AppService } from '../app.service';


@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.scss'],
  providers: [LoginService],
  animations: [FLY_IN_OUT_ANIMATION]
})
export class LoginComponent implements OnInit {

  constructor(private _loginService: LoginService,
    public snackBar: MatSnackBar,
    private _cookieService: CookieService,
    private _router: Router,
    private _appService: AppService) { }

  login(email, password) {
    if (!email) {
      email = "regular@test.com";
      password = "superSecretTok3n";
    }

    this._loginService.login(email, password)
      .subscribe(data => {
        let now = new Date();
        now.setTime(now.getTime() + 1 * 3600 * 1000);
        this._cookieService.set('token', data.token, now);
        this._appService.setUserRole(data.role);
        this.openSnackBar("Ok", "Dismiss");
        this._router.navigateByUrl('/main');  
        
        window.location.reload();
        

        setTimeout(() => {
          this._router.navigateByUrl('/main');
        },
          1500);
      },
      error => this.openSnackBar(error.json().error, "OK")
      );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


  ngOnInit() {
  }

}
