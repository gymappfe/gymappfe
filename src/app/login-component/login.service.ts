import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map'
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class LoginService {
  BASE_URL = "http://54.70.10.6/api"

  constructor(private _http: Http, private _cookieService:CookieService) {
  }

  login(email: string, password: string) {
    let credentials = {
      'email': email,
      'password': password
    }

    let body = new URLSearchParams();
    body.set('email', email);
    body.set('password', password);

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions();
    options.headers = headers;

    return this._http.post(`${this.BASE_URL}/login`, body.toString(), options)
      .map(res => res.json());
  }

}
