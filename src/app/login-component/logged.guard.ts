import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';
import { AuthGuard } from './auth.guard';

@Injectable()
export class LoggedGuard implements CanActivate {

  constructor(private _cookieService: CookieService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this._cookieService.check('token')) {
      this.router.navigate(['/main']);
      return false;
    }
    return true;
  }
}
