import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { AppConfig } from '../config/app-config';
import { deserialize } from 'json-typescript-mapper';
import { Course } from '../model/course/course';

@Injectable()
export class CoursesService {

  constructor(private _http: Http, private _cookieService: CookieService) { }

  fetch(filter: string) {
    let headers = this.configureHeaders();

    let options = new RequestOptions({
      headers: headers,
    });

    console.log(options);

    var params = '';

    if(filter){
      params = `?${filter}=true`;
    }


    return this._http.get(`${AppConfig.API_ENDPOINT}/courses` + params, options)
      .map(result => result.json());
  }

  subscribeToCourse(courseID: number) {
    let headers = this.configureHeaders();
    let options = new RequestOptions({
      headers: headers
    });

    return this._http.post(`${AppConfig.API_ENDPOINT}/course/${courseID}/subscription`, {}, options);
  }

  unsubscribeToCourse(courseID: number) {
    let headers = this.configureHeaders();
    let options = new RequestOptions({
      headers: headers
    })
    return this._http.delete(`${AppConfig.API_ENDPOINT}/course/${courseID}/subscription`, options);
  }

  private configureHeaders() {
    let token = this._cookieService.get('token');
    let headers = new Headers();
    headers.append('Authorization', `Bearer ${token}`);
    return headers;
  }

}
