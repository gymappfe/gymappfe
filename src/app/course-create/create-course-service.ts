import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { DateAdapter, MatDateFormats } from '@angular/material';


@Injectable()
export class CreateCourseService {

  BASE_URL = "http://54.70.10.6/api"
  
    constructor(private _http: Http, private _cookieService:CookieService) {
    } 
  
    createCourse(eventDate: number, capacity: string, image: File, name: string) {
      let inputs = {
        'eventDate': eventDate / 1000,
        'capacity': capacity,
        'image': image,
        'name': name 
      }

      eventDate = eventDate / 1000;
      let capacityNumber = parseInt(capacity);
      console.log(eventDate);
      console.log(typeof eventDate);
      console.log(capacity);
      console.log(typeof capacityNumber);
      console.log(image);
      console.log(typeof image);
      console.log(name);
      console.log(typeof name);
      let token = this._cookieService.get('token');
      let headers = new Headers();
      headers.append('Authorization', `Bearer ${token}`);
      let options = new RequestOptions();
      options.headers = headers;
      
      let formData = new FormData();
      formData.append("eventDate",eventDate.toString());
      formData.append("capacity", capacityNumber.toString());
      formData.append("image",image);
      formData.append("name",name);

      
      return  this._http.post(`${this.BASE_URL}/course`,formData,options).map(res => res.json());
    }

}