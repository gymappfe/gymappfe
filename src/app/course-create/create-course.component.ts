import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CreateCourseService } from './create-course-service';
import { MatSnackBar, MatDatepickerModule, MatIconModule } from '@angular/material';
import { CookieService } from 'ngx-cookie-service';
import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-course-create',
  templateUrl: './course-create.component.html',
  styleUrls: ['./course-create.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [CreateCourseService]
})
export class CourseCreateComponent implements OnInit {
  private _images: File[];

  constructor(private _createCourseService: CreateCourseService, public snackBar: MatSnackBar, private _cookieService: CookieService) { }

  createCourse(eventDate, capacity, name) {
    var image: File;
    if (this._images) {
      image = this._images[0];
    }
    // capacity = parseInt(capacity);
    eventDate = Date.parse(eventDate);
    console.log(this._cookieService.get('token'));
    this._createCourseService.createCourse(eventDate, capacity, image, name)
      .subscribe(data => this.openSnackBar("Done", "Ok"),
      error => this.openSnackBar(error.json().error, "OK")
      );

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  clearForms(){
      
  }



  onChange(event) {
    this._images = event.target.files;
    console.log(this._images);
  }

  ngOnInit() {
  }

}