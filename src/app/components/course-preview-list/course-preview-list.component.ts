import { Component, OnInit, Input } from '@angular/core';
import { CoursesService } from '../../service/courses.service';
import { Course } from '../../model/course/course';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-course-preview-list',
  templateUrl: './course-preview-list.component.html',
  styleUrls: ['./course-preview-list.component.css'],
  providers: [CoursesService]
})
export class CoursePreviewListComponent implements OnInit {
  @Input() filter;
  @Input() canUnsubscribe = true;

  private _courses: Course[];

  constructor(private _courseService: CoursesService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.fetchCourses(this.filter);
  }

  fetchCourses(filter: string) {
    this._courseService.fetch(filter)
      .subscribe(data => {
        this._courses = data;
        console.log(this._courses);
      }, error => {
        console.log("Error in fetching courses");
      })
  }

  unsubscribe(courseId: number) {
    this._courseService.unsubscribeToCourse(courseId)
      .subscribe(data => {
        this._snackBar.open("Done", "Ok");
        this._courses = this._courses.filter(course => course.id !== courseId);
      }, error => {
        this._snackBar.open("Error occured", "Dismiss");
      });
  }

}
