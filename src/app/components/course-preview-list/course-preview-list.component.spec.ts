import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursePreviewListComponent } from './course-preview-list.component';

describe('CoursePreviewListComponent', () => {
  let component: CoursePreviewListComponent;
  let fixture: ComponentFixture<CoursePreviewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursePreviewListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursePreviewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
