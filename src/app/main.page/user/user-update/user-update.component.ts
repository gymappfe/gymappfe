import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { NgForm } from '@angular/forms';
import { UserService } from '../user.service';
import { UserUpdates } from '../../../model/user/userUpdates';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css'],
  providers: [UserService]
})
export class UserUpdateComponent implements OnInit {
  private _name: string;
  private _newName: string;
  private _oldPassword: string;
  private _newPassword: string;
  private _files: File[];
  constructor(public dialogRefference: MatDialogRef<UserUpdateComponent>
    , @Inject(MAT_DIALOG_DATA) public dataBundle: any
    , private _snackBar: MatSnackBar
    , private _userService: UserService) {
    this._name = dataBundle.name;
  }
  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRefference.close();
  }

  updateProfile(updateForm: NgForm): void {
    console.log("update profile");
    let userUpdates = new UserUpdates();
    userUpdates.name = this._newName;
    userUpdates.password = this._newPassword;
    if (this._files !== undefined) {
      userUpdates.img = this._files[0];
    }

    console.log(userUpdates);

    this._userService.updateUserData(userUpdates)
      .subscribe((result) => {
        let snackBarRef = this._snackBar.open("Done", "Dismiss", {
          duration: 2000,
        });
      }, error => {
        let snackBarRef = this._snackBar.open("Error occurred!", "Dismiss", {
          duration: 2000,
        });
      })
      setTimeout(() => 
      {
        this.dialogRefference.close();
      },
      2000);
      
  }

  onChange(event) {
    this._files = event.target.files;
    console.log(this._files);
  }
}
