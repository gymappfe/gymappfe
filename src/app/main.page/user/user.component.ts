import { Component, OnInit } from '@angular/core';
import { User } from '../../model/user/user';
import { MatDialog } from '@angular/material';
import { AppConfig } from '../../config/app-config';
import { DomSanitizer } from '@angular/platform-browser';
import { UserService } from './user.service';
import { UserUpdateComponent } from './user-update/user-update.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [UserService]
})
export class UserComponent implements OnInit {
  private _user: User;
  private _baseURL: string;
  private _logoURL: string;
  constructor(private _userService: UserService
    , public updateDialog: MatDialog
    , private _domSanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.getUser();
    this._baseURL = AppConfig.SERVER_ENDPOINT;

  }

  getUser() {
    console.log("Getting user profile..")
    this._userService.getUserProfile()
      .subscribe(result => {
        this._user = result;
        console.log(this._user);
      }, error => console.log(error));
  }

  openDialog(): void {
    let dialogRef = this.updateDialog.open(UserUpdateComponent, {
      data: {
        name: this._user.fullName
      }

    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("closed");
    });
  }

}
