import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { AppConfig } from '../../config/app-config';
import { UserUpdates } from '../../model/user/userUpdates';

@Injectable()
export class UserService {

  constructor(private _http: Http, private _cookieService: CookieService) {
  }

  getUserProfile() {
    let options = this._configureHeadersOptions();

    return this._http.get(`${AppConfig.API_ENDPOINT}/user`, options)
      .map(result => result.json());
  }

  updateUserData(userUpdates: UserUpdates) {
    console.log(userUpdates);
    let fullName = userUpdates.name;
    let password = userUpdates.password;
    let picture = userUpdates.img;
    let options = this._configureHeadersOptions();

    let method = "PUT";

    let formData = new FormData();
    if (fullName !== undefined) {
      formData.append("fullName", fullName);
    }
    if (password !== undefined) {
      formData.append("password", password);
    }
    if (picture !== undefined) {
      formData.append("image", picture);
    }
    formData.append("_method", method);

    return this._http.post(`${AppConfig.API_ENDPOINT}/user`, formData, options)
      .map(result => result.json())
  }

  private _configureHeadersOptions() {
    let token = this._cookieService.get('token');
    let headers = new Headers();
    headers.append('Authorization', `Bearer ${token}`);
    let options = new RequestOptions({ headers: headers });
    return options;
  }

}
