import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { RequestOptions } from '@angular/http';
import { Http, Headers } from '@angular/http';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CounterComponent implements OnInit {

  nrOfUsersInGym: number = 0;

  constructor(private _cookieService: CookieService, private _http: Http) { }

  ngOnInit() {
    // this.setLiveCount();
    this.doJob();
  }

  BASE_URL = "http://54.70.10.6/api";

  setLiveCount() {
    this.getLiveCount().subscribe((number) => {
      this.nrOfUsersInGym = number['numberOfUsers'];
      console.log();
    }
    )
  }

  doJob() {
    setInterval(() => {
        this.setLiveCount();
    }, 5000)
  }


  getLiveCount() {
    let token = this._cookieService.get('token');

    let headers = new Headers();
    headers.append('Authorization', `Bearer ${token}`);

    let options = new RequestOptions();
    options.headers = headers;

    return this._http.get(`${this.BASE_URL}/users/at-the-gym`, options)
      .map(res => res.json());
  }

}
