import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';


@Injectable()
export class MainPageService {

    BASE_URL = "http://54.70.10.6/api"

    constructor(private _http: Http, private _cookieService: CookieService) {
    }

    getLiveCount() {
        let token = this._cookieService.get('token');

        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);

        let options = new RequestOptions();
        options.headers = headers;

        return this._http.get(`${this.BASE_URL}/users/at-the-gym`, options)
            .map(res => res.json());
    }

}
