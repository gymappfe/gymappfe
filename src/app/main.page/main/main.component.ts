import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CoursesModalComponent } from '../../courses-modal/courses-modal.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Router, ActivatedRoute, RouterStateSnapshot } from '@angular/router';
import { AppService } from '../../app.service';
import { Location } from '@angular/common';
import { MainPageService } from './main.service';



@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  providers: [Location, MainPageService]
})
export class MainComponent implements OnInit {
  private USERS_COURSES_FILTER = "usersCourses";
  private OWNED_COURSES_FILTER = "ownedCourses";
  private isLoggedOk: boolean;
  private usersInGym: number;


  constructor(public dialog: MatDialog, private router: Router, private _appService: AppService, private location: Location, private mainPageService: MainPageService) {
    // this.location = router.url;
  }

  ngOnInit() {
    this.getLiveNumberCount();
  }

  getLiveNumberCount() {
    return this.mainPageService.getLiveCount()
      .subscribe(usersNr => this.usersInGym = usersNr);
  }

  isMainPage(): boolean {
    return this.location.isCurrentPathEqualTo('/main');
  }

  isLogged() {
    this.isLoggedOk = this._appService.isLogged();
  }

  openSubscribedCoursesDialog(): void {
    let dialogRef = this.dialog.open(CoursesModalComponent, {
      height: "60%",
      width: "40%",
      data: {
        filter: this.USERS_COURSES_FILTER
      }
    });
  }

  openOwnedCoursesDialog(): void {
    let dialogRef = this.dialog.open(CoursesModalComponent, {
      height: "60%",
      width: "40%",
      data: {
        filter: this.OWNED_COURSES_FILTER
      }
    });
  }
}
