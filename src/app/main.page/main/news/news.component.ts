import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NewsService } from './news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [NewsService]
})
export class NewsComponent implements OnInit {

  newsList = [];

  constructor(private _newsService: NewsService) { }

  ngOnInit() {
    this._newsService.getNews().subscribe((news)=>{
      this.newsList = news;
      console.log(news);
      
    });


  }

}
