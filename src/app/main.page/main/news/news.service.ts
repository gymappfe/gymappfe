import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Http, RequestOptions, Headers } from '@angular/http';

@Injectable()
export class NewsService {

  BASE_URL = "http://54.70.10.6/api"


  constructor(private _http: Http, private _cookieService: CookieService) { }

  getNews() {
    let token = this._cookieService.get('token');

    let headers = new Headers();
    headers.append('Authorization', `Bearer ${token}`);

    let options = new RequestOptions();
    options.headers = headers

    return this._http.get(`${this.BASE_URL}/news`, options)
      .map(res => res.json());
  }

}
