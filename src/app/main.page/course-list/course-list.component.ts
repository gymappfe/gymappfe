import { Component, OnInit, Input } from '@angular/core';
import { CoursesService } from '../../service/courses.service';
import { deserialize } from 'json-typescript-mapper';
import { MatSnackBar } from '@angular/material';
import { Course } from '../../model/course/course';
import { AppConfig } from '../../config/app-config';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css'],
  providers: [CoursesService]
})
export class CourseListComponent implements OnInit {
  private courses: Course[];
  private allCourses: Course[];
  private _baseURL: string;
  @Input() filter;

  constructor(private _coursesService: CoursesService
    , private _snackBar: MatSnackBar) {
    // this.allCourses =  this.courses;

  }

  ngOnInit() {
    this._baseURL = AppConfig.SERVER_ENDPOINT;
    this.fetch(this.filter);
  }

  fetch(filter: string): void {
    console.log(filter);
    this._coursesService.fetch(filter)
      .subscribe(data => {
        this.courses = data;
        this.allCourses = data;
        this.all();
        console.log(this.courses);
      }, error => console.log(error))
  }

  all() {
    let cursuriFiltrare = this.allCourses.filter((course) => {
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth() + 1; //January is 0!
      var yyyy = today.getFullYear();
      today.setHours(0, 0, 0, 0);

      let dateOfCourse = new Date(parseInt(course.eventDate) * 1000);
      // dateOfCourse.setFullYear(2018);
      // console.log(dateOfCourse);


      return today < dateOfCourse;
    });

    this.courses = cursuriFiltrare;
  }

  tomorrow() {
    let cursuriFiltrare = this.allCourses.filter((course) => {
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth() + 1; //January is 0!
      var yyyy = today.getFullYear();
      today.setHours(0, 0, 0, 0);

      let dateOfCourse = new Date(parseInt(course.eventDate) * 1000);

      if (yyyy === dateOfCourse.getFullYear() && mm === dateOfCourse.getMonth() && dd+1 === dateOfCourse.getDate()) {
        return true;
      }
      return false;
    });

    this.courses = cursuriFiltrare;
  }

  thisWeek() {
    let cursuriFiltrare = this.allCourses.filter((course) => {

      var curr = new Date; // get current date
      var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
      var last = first + 6; // last day is the first day + 6

      var firstday = new Date(curr.setDate(first));
      var lastday = new Date(curr.setDate(last));

      let dateOfCourse = new Date(parseInt(course.eventDate) * 1000);
      if (firstday <= dateOfCourse && lastday >= dateOfCourse) {
        return true;
      }
      return false;
    });

    this.courses = cursuriFiltrare;
  }

  subscribeToCourse(courseID: number): void {
    console.log("Subscribing to course " + courseID);
    this._coursesService.subscribeToCourse(courseID)
      .subscribe(result => {
        this._snackBar.open("Done", "Dismiss");
      }, error => {
        let errorJson: JSON;
        errorJson = error._body;
        this._snackBar.open(`Error occured`, "Dismiss");
      });
  }

  unSubscribeToCourse(courseID: number): void {
    console.log("Unsubscribing to course " + courseID);
    this._coursesService.unsubscribeToCourse(courseID)
      .subscribe(result => {
        this._snackBar.open("Done", "Dismiss");
      }, error => {
        this._snackBar.open("Error", "Dismiss");
      });
  }
}
