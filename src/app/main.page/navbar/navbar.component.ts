import { Component, Compiler, Input, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { UserService } from '../user/user.service';
import { AppService } from '../../app.service';
import { User } from '../../model/user/user';
import { AppConfig } from '../../config/app-config';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'navbar',
  templateUrl: 'navbar.component.html',
  providers: [UserService,AppService]

})
export class NavbarComponent {
  private _user: User;
  private _baseURL: string;
  private isLoggedOk:boolean;
  private token:string;
  @Input()  sidenavRef;
  
  constructor(private _compiler: Compiler,private _userService: UserService,private _appService: AppService,private _cookieService: CookieService,private router: Router){
      console.log(this.sidenavRef);
   
  }

  getUser() {
    console.log("Getting user profile..")
    this._userService.getUserProfile()
      .subscribe(result => {
        this._user = result;
        console.log(this._user);
      }, error => console.log(error));
  }
  isLogged(){
    this.isLoggedOk=this._appService.isLogged();
  }
  logout(){
  /*  this._cookieService.delete('token');
   this._cookieService.delete('UserRole'); */
   

   this._cookieService.deleteAll('/');    
   this.router.navigate(['login']);
   window.location.reload();
   
  
  }
  ngOnInit() {
    this.isLogged();
    if(this.isLoggedOk==true){
      this.getUser();
      this._baseURL = AppConfig.SERVER_ENDPOINT;
    }

  }

} 
