import { TestBed, inject } from '@angular/core/testing';


import { ProductsService } from './products.service';
import { MatDialog } from '@angular/material';

describe('CoursesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductsService, MatDialog]
    });
  });

  it('should be created', inject([ProductsService], (service: ProductsService) => {
    expect(service).toBeTruthy();
  }));
});
