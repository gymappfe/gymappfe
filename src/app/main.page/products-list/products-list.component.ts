import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import {MatGridListModule} from '@angular/material/grid-list';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { deserialize } from 'json-typescript-mapper';
import { ProductsService } from './products.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Product } from '../../model/product/product';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { AppConfig } from '../../config/app-config';


@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [ProductsService, MatDialog]
})
export class ProductsListComponent implements OnInit {

  private products: Product[];
  private productIds: number[];
  private _baseURL : string;

  public show: boolean = false;
  
    constructor(private _coursesService:ProductsService,
                public productDetailsDialog : MatDialog) { }

    openDialog(index){
     
      console.log(this.products[index]);
      this.show  =!this.show;  
          let dialogRef = this.productDetailsDialog.open(ProductDetailsComponent, {
            height: "75%", width: "75%",
            data: {
              name: this.products[index].name,
              description: this.products[index].description,
              price: this.products[index].price,
              category: this.products[index].category,
              image: this.products[index].image,
            }
      
          });
      
          dialogRef.afterClosed().subscribe(result => {
            console.log("closed");
          });
  
    }
        
    ngOnInit() {
        this._baseURL = AppConfig.SERVER_ENDPOINT;
        this._coursesService.getAll()
          .subscribe(data=>{
            this.products = data;
            console.log(this.products);
          }, error => console.log(error))
  
    }
}
