import { Component, OnInit, ViewEncapsulation, Inject, Optional } from '@angular/core';
import { MatDialog,MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppConfig } from '../../../config/app-config';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductDetailsComponent implements OnInit {

  private _id: number;
  private _price: number;
  private _description: string;
  private _image: File;
  private _name: string;
  private _category: string;
  private _baseURL: string;

  constructor(public dialogRefference: MatDialogRef<ProductDetailsComponent>
    , @Optional() @Inject(MAT_DIALOG_DATA) public dataBundle: any) {
      this._name = dataBundle.name;
      this._description = dataBundle.description;
      this._category = dataBundle.category;
      this._price  = dataBundle.price;
      this._image = dataBundle.image;
  }

  ngOnInit() {
    this._baseURL = AppConfig.SERVER_ENDPOINT; 
  }

  onNoClick(): void {
    this.dialogRefference.close();
  }

}
