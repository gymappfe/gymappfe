import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { deserialize } from 'json-typescript-mapper';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppConfig } from '../../config/app-config';


@Injectable()
export class ProductsService {

  constructor(private _http: Http, private _cookieService: CookieService) { }

  getAll() {

    let token = this._cookieService.get('token');
    let headers = new Headers();
    headers.append('Authorization', `Bearer ${token}`);
    let options = new RequestOptions({ headers: headers });

    return this._http.get(`${AppConfig.API_ENDPOINT}/products`, options)

      .map(result => result.json());
  }

}
